# README #

* This project is Arduino code which allows an Arduino Mega with an Ethernet shield to run as an 8 output GE Colour Effects DMX controller.
* 
* The only file you typically need to change is global.h which has all the setting such as IP Address, Universes etc
*
*  Supported Ethernet controllers: 
*    * W5100
*    * W5200
*    * ENC28J60
*
* Selecting an Ethernet controller, the ENC28J60 or W5200 have higher performance than the W5100, which is not able to handle as many
* connections at the same time and also has less memory for storing packets.
*
* The Ethernet interface implements Streaming-ACN (E1.31), DMX over ethernet.  It supports both standard and a modified minimal header
* format for reduced overhead.  Note that only unicast mode is working, multicast is not (yet) functional.
*
*
* Driving the GE G35 lights is a challenge because they are so slow. It takes almost a millisecond to send instructions to a bulb.
* Compare this to WS2811 which is about 10us.
* 
* The GE G35 strands can be connected to any of the digital ports, by default, it uses PORTA, PINs 22-29, where the FIRST DMX addresses
* are assigned to PIN 29, the LAST addresses on PIN 22.  The digital port used can be changed in debug.h if desired.
* 
* The program is highly configurable, allowing up to 8 strands to be defined, with a flexible number of bulbs per strand.  The default
* is 8 strands of 50 bulbs, but a higher refresh rate can be achieved by reducing the number of bulbs per strand if less than 50 are
* used.