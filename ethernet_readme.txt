W5100 or W5200

You should replace the entire Ethernet folder with the code in the Ethernet_KW_ENHANCED file. This enhanced version of the library allows us to take full advantage of the W5100/W5200 receive buffers by allocating them all to the first socket. It also adds some debugging routines when you have ethernet issues.

ENC28J60

Install my version of UIPEthernet library. It has a few functions that speed internet packet processing.