#include "global.h"
#include "MyEthernet.h"
#include "ethernetdebug.h"
#include "socketdebug.h"
#ifdef USEENC28J60
#else
	#include <utility/w5100.h>
#endif

byte mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, IP_BYTE_4};
IPAddress ip(IP_BYTE_1,IP_BYTE_2,IP_BYTE_3,IP_BYTE_4); 
IPAddress debugIP(IP_BYTE_1,IP_BYTE_2,IP_BYTE_3,DEBUG_IP_BYTE_4); 

#ifdef MULTICAST
	IPAddress ipMulti(MULTIIP_BYTE_1, MULTIIP_BYTE_2, MULTIIP_BYTE_3, MULTIIP_BYTE_4);
#endif

#ifdef RESETW5200
	#define RST 8
	#define PWDN 9
	#define INT 3
#endif

MyEthernet::~MyEthernet()
{
	#ifdef DEBUGSERIAL
		Serial.println(F("What the ... MyEthernet being deleted."));
		Serial.flush();
	#endif
}

MyEthernet::MyEthernet()
{
	#ifdef MULTIUDP
		_lastUDP = UDPUNIVERSES - 1;
	#endif

	bool ethernetok = false;
	bool errorflash = false;

	#ifdef RESETW5200
		pinMode(RST,OUTPUT);
		pinMode(PWDN,OUTPUT);
		pinMode(INT,INPUT);
	#endif

	#ifdef MANUALW5X00RESETPIN
		pinMode(MANUALW5X00RESETPIN,OUTPUT);
	#endif

	while(!ethernetok)
	{
		#ifdef RESETW5200
			// This code may reset the W5200 chip ... and I hope wont hurt the W5100 chip
			digitalWrite(PWDN,LOW); //enable power
			digitalWrite(RST,LOW);  //Reset W5200
			delay(10);
			digitalWrite(RST,HIGH);  
			delay(200);      		// wait W5200 work
		#endif

		#ifdef MANUALW5X00RESETPIN
			digitalWrite(MANUALW5X00RESETPIN,LOW);  //Reset W5X00
			delay(100);
			digitalWrite(MANUALW5X00RESETPIN,HIGH);  
		#endif
		
		#ifdef DEBUGSERIAL
			Serial.println(F("Initialising ethernet ..."));
			Serial.print(F("MAC Address {"));
			for(int i =0; i < sizeof(mac)-1; i++)
			{
				Serial.print(mac[i], HEX);
				Serial.print(F(","));
			}
			Serial.print(mac[sizeof(mac)-1], HEX);
			Serial.println(F("}"));
			Serial.print(F("Requested IP Address "));
			Serial.println(ip);
			Serial.flush();
		#endif
		
		delay(1000);
		
		Ethernet.begin(mac, ip);
		
		// delay to let IP address take effect? Sounds bogus but I am desperate
		delay(100);

		while (!Ethernet.linkStatus())
		{
			#ifdef DEBUGSERIAL
				Serial.println(F("Waiting for link to come up."));
			#endif
			delay(200);
		}

		#ifdef CHANGERTCTIMINGS
			#ifdef DEBUGSERIAL
				Serial.println(F("Setting non-standard RTR and RCR"));
			#endif
			Ethernet.SetRTRRCR(200, 0);
		#endif

		#ifdef DEBUGSERIAL
			#ifdef USEENC28J60
			Serial.println(F("ENC28J60 in use"));
			#else
			Serial.print(F("MAX_SOCK_NUM = "));
			Serial.println(W5100.getMaxSockets());
			#endif
			
			Serial.print(F("Allocated IP Address "));
			Serial.println(Ethernet.localIP());
		#endif
	
		if (Ethernet.localIP() == ip)
		{
			ethernetok = true;
		}
		else
		{
			Utility::Debugln(F("Ethernet did not initialise correctly ... trying again."));
			
			#ifdef DEBUGSERIAL
				ethernetDumpState(3);
			#endif
			
			errorflash = ! errorflash;
			if (errorflash)
			{
				digitalWrite(ERRORPIN, 1);
			}
			else
			{
				digitalWrite(ERRORPIN, 0);
			}
		}
	}
	
	// set large receive buffer
	#ifdef USEENC28J60
		// This is done in ENC28J60.h
	#else
		for (int i = 1; i < W5100.getMaxSockets(); i++) 
		{
			SetRXBufferSize(i, 0);
		}
		if (W5100.getMaxSockets() == 8)
		{
			SetRXBufferSize(0, 16);
		}
		else
		{
			SetRXBufferSize(0, 8);
		}
	#endif
}

void MyEthernet::Dump()
{
	#ifdef DEBUGSERIAL
		ethernetDumpState(3);
		#ifdef USEENC28J60
			socketDumpState(0, 3);
		#else
			socketDumpState(_Udp.sockethandle(), 3);
		#endif
	#endif
}

void MyEthernet::Dump(SOCKET s)
{
	#ifdef DEBUGSERIAL
		socketDumpState(s, 3);
	#endif
}

void MyEthernet::SetDebugIP(IPAddress newip)
{
	debugIP = newip;
}

void MyEthernet::SendDebugPacket(char* p)
{
	_sendUdp.beginPacket(debugIP, DEBUGPORT);
	_sendUdp.write(p, strlen(p));
	_sendUdp.endPacket();
	
	#ifdef DEBUGSERIAL
		Serial.println(F("DEBUG packet sent."));
	#endif
}

void MyEthernet::SendAckPacket(byte* p, short size)
{
	_sendUdp.beginPacket(debugIP, DEBUGPORT);
	_sendUdp.write(p, size);
	int rc = _sendUdp.endPacket();

	#ifdef DEBUGSERIAL
		Serial.println(F("ACK packet sent."));
	#endif
}

void MyEthernet::SendNotifyPacket(byte notify)
{
	IPAddress ip(IP_BYTE_1,IP_BYTE_2,IP_BYTE_3,notify); 
	_sendUdp.beginPacket(ip, DEBUGPORT);
	char buffer[] = "NOTIFY";
	_sendUdp.write(&buffer[0], sizeof(buffer));
	int rc = _sendUdp.endPacket();

	#ifdef DEBUGSERIAL
		Serial.println(F("NOTIFY packet sent."));
	#endif
}

IPAddress MyEthernet::RemoteIP()
{
	#ifdef MULTIUDP
	return _Udps[0].remoteIP();
	#else
	return _Udp.remoteIP();
	#endif
}

IPAddress MyEthernet::LocalIP()
{
	return Ethernet.localIP();
}

void MyEthernet::Reset()
{
	#ifdef DEBUGSERIAL
		Serial.println(F("MyEthernet ... resetting."));
		ethernetDumpState(3);
	#endif

	#ifdef MULTIUDP
		for(byte i = 0; i < UDPUNIVERSES; i++)
		{
			_Udps[i].stop();
		}
	#else
		_Udp.stop();
	#endif
	StartUDP();
	#ifdef DEBUGSERIAL
		Serial.println(F("Restarted."));
		ethernetDumpState(3);
	#endif
}

int MyEthernet::ParsePacket()
{
	#ifdef MULTIUDP
		int size = 0;
		// special case of just one universe
		if (UDPUNIVERSES - 1 == 0)
		{
			#ifdef DEBUGSERIAL
				//Serial.print(F("Checking UDP "));
				//Serial.println(0);
			#endif
			size = _Udps[0].parsePacket();
			if (size == 0)
			{
				// no packet found
			}
			#ifdef DEBUGSERIAL
			else
			{
				Serial.print(F("Found packet on UDP "));
				Serial.print(0);
				Serial.print(F(" remote port "));
				Serial.println(_Udps[0].remotePort());
			}
			#endif
		}
		else
		{
			int current = _lastUDP+1;
			if (current == UDPUNIVERSES)
			{
				current = 0;
			}
			int last = current;
			
			do
			{
				#ifdef DEBUGSERIAL
					Serial.print(F("Checking UDP "));
					Serial.println(current);
				#endif
				size = _Udps[current].parsePacket();
				if (size == 0)
				{
					current++;
					if (current == UDPUNIVERSES)
					{
						current = 0;
					}
				}
				#ifdef DEBUGSERIAL
				else
				{
					Serial.print(F("Found packet on UDP "));
					Serial.print(current);
					Serial.print(F(" remote port "));
					Serial.println(_Udps[current].remotePort());
				}
				#endif
			}
			while (size == 0 && current != last);
			
			if (size != 0)
			{
				_lastUDP = current;
			}
		}

		return size;
	#else
		return _Udp.parsePacket();
	#endif
}

int MyEthernet::Read(unsigned char* p, size_t size)
{
	#ifdef MULTIUDP
	return _Udps[_lastUDP].read(p, size);
	#else
	return _Udp.read(p, size);
	#endif
}

int MyEthernet::Skip(size_t size)
{
	#ifdef MULTIUDP
	return _Udps[_lastUDP].skip(size);
	#else
	return _Udp.skip(size);
	#endif
}

int MyEthernet::Available()
{
	#ifdef MULTIUDP
	return _Udps[_lastUDP].available();
	#else
	return _Udp.available();
	#endif
}

void MyEthernet::Flush()
{
	#ifdef MULTIUDP
	return _Udps[_lastUDP].flush();
	#else
	return _Udp.flush();
	#endif
}

void MyEthernet::StartUDP()
{
	#ifdef MULTIUDP
		#ifdef MULTICAST
			#ifdef DEBUGSERIAL
			Serial.println(F("Multicast NOT IMPLEMENTED"));
			#endif
			//_Udp.beginMulti(PORT, ipMulti);
		#else
			Serial.println(F("Unicast"));
			for (byte i = 0; i < UDPUNIVERSES; i++)
			{
				if (_Udps[i].begin(PORT) == 0)
				{
					#ifdef DEBUGSERIAL
						Serial.print(F("Error starting UDP ... Connection: "));
						Serial.println(i);
					#endif
					Utility::Error(3);
				}
				else
				{
					#ifdef DEBUGSERIAL
						Serial.print(F("UDP Listening ... Connection: "));
						Serial.println(i);
					#endif
				}
			}
		#endif
	#else
		#ifdef MULTICAST
			Serial.println(F("Multicast NOT IMPLEMENTED"));
			//_Udp.beginMulti(PORT, ipMulti);
		#else
			Serial.println(F("Unicast"));
			if (_Udp.begin(PORT) == 0)
			{
				#ifdef DEBUGSERIAL
					Serial.print(F("Error starting UDP ... Connection: "));
					Serial.println(i);
				#endif
				Utility::Error(3);
			}
			else
			{
				#ifdef DEBUGSERIAL
					Serial.println(F("UDP Listening ..."));
				#endif
			}
		#endif
	#endif

	#ifdef DEBUGSERIAL
		ethernetDumpState(3);
		ethernetDumpAllSockets(3);
	#endif
	
	#ifdef NOTIFY_1_IP_BYTE_4
		SendNotifyPacket(NOTIFY_1_IP_BYTE_4);
	#endif
	#ifdef NOTIFY_2_IP_BYTE_4
		SendNotifyPacket(NOTIFY_2_IP_BYTE_4);
	#endif
	#ifdef NOTIFY_3_IP_BYTE_4
		SendNotifyPacket(NOTIFY_3_IP_BYTE_4);
	#endif
	#ifdef NOTIFY_4_IP_BYTE_4
		SendNotifyPacket(NOTIFY_4_IP_BYTE_4);
	#endif
	#ifdef NOTIFY_5_IP_BYTE_4
		SendNotifyPacket(NOTIFY_5_IP_BYTE_4);
	#endif
}