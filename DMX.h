#ifndef DMX_H
	#define DMX_H

	class DMX
	{
		short _currentPacketUniverse;
		byte _sequence;

		void skipbytes(short bytes);
		bool checkbyte(byte b);
		bool checkword(short w);
		bool checklong(long l);
		long readlong();
		byte readbyte();
		short readword();
		bool ValidUniverse();
		bool readbytes(byte* p, unsigned int size);

		#ifdef USEFULLPACKETREADIFPOSSIBLE
		byte* _packetBuffer;
		short _packetBufferSize;
		short _packetBufferUsedSize;
		short _packetBufferByte;
		bool LoadPacketIntoBuffer(short size);
		#endif

		public:
		DMX();
		bool PreparePacket(bool skip2, int packetSize);
		byte Read();
		bool Read(byte* p, unsigned int size);
		short ReadWord();
		void Flush();
		inline short Universe() { return _currentPacketUniverse; };
		short Left();
		byte Sequence() { return _sequence; };
	};

#endif