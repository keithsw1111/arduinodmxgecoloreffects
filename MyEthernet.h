#ifndef MYETHERNET_H
	#define MYETHERNET_H

	#include "global.h"
	#ifdef USEENC28J60
		#include <UIPEthernet.h>
		#define SOCKET uint8_t
	#else
    	#include <SPI.h>
		#include <Ethernet.h>
		#include <EthernetUdp.h>
		#include <utility/socket.h>
	#endif

	// If using an ENC28J60 board to an arduino mega then connect it as follows:
	// CS - 53
	// SI - 51
	// SO - 50
	// SCK - 52
	// GND - GND
	// 5V - 5V or 3.3V - 3.3V
	
	class MyEthernet
	{
		#ifdef MULTIUDP
			#ifdef OUTPUTSTRINGS
				#ifdef RELAYUNIVERSE
					#define UDPUNIVERSES OUTPUTSTRINGS+1
				#else
					#define UDPUNIVERSES OUTPUTSTRINGS
				#endif
			#else
				#define UDPUNIVERSES 1
			#endif
			EthernetUDP _Udps[UDPUNIVERSES];
			byte _lastUDP;
		#else
			EthernetUDP _Udp;
		#endif
		EthernetUDP _sendUdp;
		void SendNotifyPacket(byte notify);
		void Startup();

		public:
		// An EthernetUDP instance to let us send and receive packets over UDP
		MyEthernet();
		~MyEthernet();
		void Reset();
		void StartUDP();
		void SetDebugIP(IPAddress newip);
		void SendDebugPacket(char* p);
		void SendAckPacket(byte* p, short size);
		void Dump();
		void Dump(SOCKET s);
		IPAddress RemoteIP();
		IPAddress LocalIP();
		int ParsePacket();
		int Read(unsigned char* p, size_t size);
		int Skip(size_t size);
		int Available();
		void Flush();
	};

#endif