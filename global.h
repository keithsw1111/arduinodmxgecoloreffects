#ifndef GLOBAL_H
	#define GLOBAL_H
	
	// Choose the type of ethernet chip you are using
	#define USEENC28J60
	//#define USEW5100
	//#define USEW5200
	
	#include "debug.h"
	
	// this is the default colour displayed if the controller does not receive a packet in a long time
	#define DEFAULTCOLOUR RED
	
	// if this is defined then the board will reboot if it does not receive a network packet for the specified timeout interval DEFAULTONTIME
	// If not defined then it will just delete and recreate the network connection
	// define it if once losing connections your controller does not reconnect automatically
	//#define REBOOTONPACKETTIMEOUT 1
	
	// disable start pattern which is defined in debug.h
	//#undef STARTPATTERN

	// If no packet is seen for this long then everything turns on/goes white
	#define DEFAULTONTIME 30
	
	// first universe ... also determines IP address
	#define UNIVERSE 2

	// length of longest string ... code is faster if this is shorter and less memory used
	#define MAXBULBS 50 

	// Number of strings ... code is faster if this is smaller and less memory used
	#define OUTPUTSTRINGS 8 

	// This is the number of physical pixels that a DMX pixel actually impacts ... typically 1
	#define PHYSICALTOLOGICALRATIO 1
	
	// this is the IP Address of the arduino
	#define IP_BYTE_1 192
	#define IP_BYTE_2 168
	#define IP_BYTE_3 0
	#define IP_BYTE_4 231
	
	// large numbers lead to long bulb output ... shorter keeps more on top of network traffic but bulbs may not update often enough
	#define MAXPACKETSBETWEENDISPLAY 8
	
	// Maximum network packet checking loops with no bulb updates before we just refresh the bulbs for the heck of it
	// -1 disables this
	#define MAXLOOPSBETWEENBULBUPDATES -1
	
#endif