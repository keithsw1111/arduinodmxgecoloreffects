#ifndef DEBUG_H
	#define DEBUG_H
	
	#if defined(ARDUINO) && ARDUINO >= 100
		#include "Arduino.h"
	#else
		#include "WProgram.h"
	#endif
	#include <avr/pgmspace.h>

	extern byte debug1;
	extern byte debug2;
	extern byte ethtest;
	extern byte debugudp;
	extern int maxpacketsbetweendisplay;
	extern int maxloopsbetweenbulbupdates;
	class MyEthernet;
	extern MyEthernet* myethernet;
	
	#define WHITE 128,128,128
	#define RED 255,0,0
	#define GREEN 0,255,0
	#define BLUE 0,0,255
	#define BLACK 0,0,0
	#define YELLOW 255,255,0
	#define CYAN 0,255,255
	#define MAGENTA 255,0,255
	#define ON 1
	#define OFF 0
	
	// define this to display debug messages via the serial port
	//#define DEBUGSERIAL

	// When defined the normal 125 byte header is reduced to 4 bytes ... saves network data and thus makes the
	// program faster but requires a program to strip it down
	//#define MINIMALHEADER
	
	// When defined the program will attempt to read into memory the entire packet if sufficient memory is available. 
	// This really only makes a difference on the W5200 which has a more efficient SPI protocol when loading lots of bytes
	#define USEFULLPACKETREADIFPOSSIBLE
		

	// When defined more aggressive timout settings are used
	#ifdef USEW5200
		//#define CHANGERTCTIMINGS
	#endif
	
	//This does not work!!!
	//#define MULTICAST

	// This is the port we will listen to
	#define PORT 5568

	// Define TRACKCHANGES to track bulb changes and minimise bit manipulation
	#define TRACKCHANGES
	
	// define this to make one universe = 1 output otherwise it tries to pack multi outputs into 1 universe
	#define ONEUNIVERSEPEROUTPUT

	// start channel in all universes for data
	#define STARTCHANNEL 1

	// these are the last octet of the machine to notify when we start ... you can have up to 5
	//#define NOTIFY_1_IP_BYTE_4 104
	//#define NOTIFY_2_IP_BYTE_4 228
	//#define NOTIFY_3_IP_BYTE_4 229
	//#define NOTIFY_4_IP_BYTE_4 104
	//#define NOTIFY_5_IP_BYTE_4 104
	
	// this is the last octet of the machine to send debug data to
	#define DEBUG_IP_BYTE_4 111
	
	// this is the port debug commands are received on and sent to
	#define DEBUGPORT 22222

	// this is not used as multicast has not been enabled
	#define MULTIIP_BYTE_1 239
	#define MULTIIP_BYTE_2 255 
	#define MULTIIP_BYTE_3 0 
	#define MULTIIP_BYTE_4 UNIVERSE

	// Define this as 1 and it will send debug messages to port 22222 to the machine on xxx.xxx.xxx.DEBUG_IP_BYTE_4
	#define DEBUGUDP 0 

	// start pattern ... define this to make the program flash the lights on startup
	#define STARTPATTERN 1

	// define this to do deep packet validation ... this is computationally intensive
	// #define DETAILEDPACKETCHECKING 

	// ARDUIO PORT to output data to
	// PORTA - pins 22-29 DDRA
	// PORTC - pins 37-30 DDRC
	// PORTF - pins 54-61 DDRD
	// PORTK - pins 62-69 DDRK
	// PORTL - pins 49-42 DDRL
	#define PINPORT PORTA 
	#define DIRECTIONREGISTER DDRA

	// pin debug button 1 is attached to ... attach a button that pulls this pin to low as the program starts and it will move to that debug state.
	#define DEBUGBUTTON1 54 
	// pin debug button 2 is attached to ... attach a button that pulls this pin to low as the program starts and it will move to that debug state.
	#define DEBUGBUTTON2 55
	// progress led pin ... attach an LED and it will flash as it works
	#define PROGRESSPIN 56 // 13
	// error led pin ... attach an LED and it will flash if it hits a fatal problem
	#define ERRORPIN 57 // 12
		
	// forces the W5200 to restart on boot ... This assumes the board connects these to the W5x00 Reset pin which 
	// I am not sure it does.
	#ifdef USEW5200
		#define RESETW5200 1
	#endif
	
	// When defined this pin is pulled low when the ethernet is started.
	// If you detach the reset from the normal reset pins on both ICMP and the normal pins and connect it
	// manually to this pin then we can manually reset the W5x00 after the arduino has booted.
	//#define MANUALW5X00RESETPIN 9
	
	#ifdef USEENC28J60
		#define MULTIUDP
	#endif
	
	#include "utility.h"

#endif