#ifndef DISPLAY_H
	#define DISPLAY_H
	
	#include "DMX.h"
	#include "BulbTracker.h"
	
	class Display
	{
		DMX _dmx;
		BulbTracker _bulbTracker;
		bool CheckStateChange(bool skip2, int packetSize);
		
		public:
		Display();
		void RouteDMX();
		void Process();
		void RunDebug(byte debug);
		void RunEthernetTest(byte testtype);
	};
#endif