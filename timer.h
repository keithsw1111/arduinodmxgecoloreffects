#ifndef TIMER_H
	#define TIMER_H
	
	class Timer
	{
		unsigned long _start;
		char* _name;
	
		public:
		Timer(char* name);
		~Timer();
		void DumpSerial();
		void Reset();
		unsigned long elapsed();
	};
#endif