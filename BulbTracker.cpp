#include "global.h"
#include "BulbTracker.h"


// these allow us to optionally pack > 1 string in a universe
// basically it puts as many whole strings into a universe as fits
#ifdef ONEUNIVERSEPEROUTPUT
	#define _UNIVERSES OUTPUTSTRINGS
	#define _STRINGSPERUNIVERSE 1
	#define _STRINGSINLASTUNIVERSE 1
#else
	#define _UNIVERSES ((OUTPUTSTRINGS + (512/(MAXBULBS * 3)))/ (512/(MAXBULBS * 3)))
	#define _STRINGSPERUNIVERSE (512/(MAXBULBS*3))
	#define _STRINGSINLASTUNIVERSE (OUTPUTSTRINGS - ((_UNIVERSES-1)*_STRINGSPERUNIVERSE))
#endif
#define _LASTUNIVERSE(x) ((x-UNIVERSE) == _UNIVERSES-1)

BulbTracker::BulbTracker()
{
	Utility::Debugln(F("BulbTracker create."));
	Utility::Debug(F("_UNIVERSES "));
	Utility::Debugln(_UNIVERSES);
	Utility::Debug(F("_STRINGSPERUNIVERSE "));
	Utility::Debugln(_STRINGSPERUNIVERSE);
	Utility::Debug(F("_STRINGSINLASTUNIVERSE "));
	Utility::Debugln(_STRINGSINLASTUNIVERSE);

	// set output pins
	DIRECTIONREGISTER = 0xFF;

	_pBulbData = (byte*)malloc(MAXBULBS * OUTPUTSTRINGS * 3);
	memset(_pBulbData, 0x00, MAXBULBS * OUTPUTSTRINGS * 3);

	//Utility::Debug(F("BulbTracker::Bulbs data array allocated "));
	//Utility::Debugln((long)_pBulbData, HEX);

	// start bit
	_pSendBuffer[0] = 0xFF;

	// brightness 0xC0 - lets not overdo it
	_pSendBuffer[7] = 0xFF;
	_pSendBuffer[8] = 0xFF;
	_pSendBuffer[9] = 0x00;
	_pSendBuffer[10] = 0x00;
	_pSendBuffer[11] = 0x00;
	_pSendBuffer[12] = 0x00;
	_pSendBuffer[13] = 0x00;
	_pSendBuffer[14] = 0x00;

	#ifdef TRACKCHANGES
		// All bulbs initially need to be sent
		SetAllDirty();
	#endif
		
	Utility::Debugln(F("BulbTracker create done!"));
}
		
void BulbTracker::DumpBulbData()
{
	Utility::Debugln(F("Bulb data ... dumped by OUTPUTSTRINGS."));
	for (byte j = 0; j < OUTPUTSTRINGS; j++)
	{
		Utility::Debug(F("String "));
		Utility::Debug(j);
		Utility::Debug(F(" Universe "));
		Utility::Debug(UNIVERSE + j);
		Utility::Debug(F(" Bulb Data "));
		byte* p = _pBulbData + j;

		for (byte i = 0; i < MAXBULBS; i++)
		{
			Utility::Debug(F("("));
			Utility::Debug(*(p+i*3*OUTPUTSTRINGS+OUTPUTSTRINGS+OUTPUTSTRINGS)&0xF0>>BITSPERCOLOUR, HEX);
			Utility::Debug(F(","));
			Utility::Debug(*(p+i*3*OUTPUTSTRINGS+OUTPUTSTRINGS)&0xF0>>BITSPERCOLOUR, HEX);
			Utility::Debug(F(","));
			Utility::Debug(*(p+i*3*OUTPUTSTRINGS)&0xF0>>BITSPERCOLOUR, HEX);
			Utility::Debug(F(") "));
		}
		Utility::Debugln(F(""));
	}
}
void BulbTracker::DumpSendBuffer()
{
	Utility::Debug(F("Send buffer: "));
	for (byte i = 0; i < 1 + 6 + 8 + 3 * BITSPERCOLOUR; i++)
	{
		Utility::Debug(*(_pSendBuffer+i), HEX);
		Utility::Debug(F(" "));
	}
	Utility::Debugln(F(""));
}

#ifdef TRACKCHANGES
void BulbTracker::DumpTracker()
{
	Utility::Debugln(F("Bulb clean/Dirty flags ... 1 per bulb ... '1' if that bulb on any string has been updated."));
	for(byte i=0; i<MAXBULBS;i++)
	{
		Utility::Debug(_tracker[i]);
		Utility::Debug(F(" "));
	}
	Utility::Debugln(F(""));
}
#endif

void BulbTracker::Dump()
{
	DumpBulbData();
	#ifdef TRACKCHANGES
	DumpTracker();
	#endif
	DumpSendBuffer();
}

void BulbTracker::SetBulbColour(byte r, byte g, byte b, byte string, short bulb)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("BulbTracker::SetBulbColour. String "));
		//Utility::Debug(string);
		//Utility::Debug(F(" Bulb "));
		//Utility::Debug(bulb);
		//Utility::Debug(F(" ("));
		//Utility::Debug(r, HEX);
		//Utility::Debug(F(","));
		//Utility::Debug(g, HEX);
		//Utility::Debug(F(","));
		//Utility::Debug(b, HEX);
		//Utility::Debugln(F(")"));
	#endif

	#ifdef TRACKCHANGES
		// our bulb will change
		SetDirty(bulb);
	#endif

	byte* p = _pBulbData + string + (bulb * OUTPUTSTRINGS * 3);
	*p = b;
	*(p+OUTPUTSTRINGS) = g;
	*(p+OUTPUTSTRINGS+OUTPUTSTRINGS) = r;

	#ifdef DEBUGSERIAL
		//DumpBulbData();
	#endif
}

void BulbTracker::SetStringColour(byte r, byte g, byte b, byte string)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("BulbTracker::SetStringColour. String "));
		//Utility::Debug(string);
		//Utility::Debug(F(" ("));
		//Utility::Debug(r, HEX);
		//Utility::Debug(F(","));
		//Utility::Debug(g, HEX);
		//Utility::Debug(F(","));
		//Utility::Debug(b, HEX);
		//Utility::Debugln(F(")"));
	#endif
	
	#ifdef TRACKCHANGES
		// make them all dirty ... every bulb will change
		SetAllDirty();
	#endif

	byte* p = _pBulbData + string;
	for (byte i = 0; i < MAXBULBS; i++)
	{

		*p = b;
		*(p+OUTPUTSTRINGS) = g;
		*(p+OUTPUTSTRINGS+OUTPUTSTRINGS) = r;
		p += OUTPUTSTRINGS * 3;
	}				

	#ifdef DEBUGSERIAL
		//DumpBulbData();
	#endif
}

void BulbTracker::SetAllColour(byte r, byte g, byte b)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("BulbTracker::SetAllColour. ("));
		//Utility::Debug(r, HEX);
		//Utility::Debug(F(","));
		//Utility::Debug(g, HEX);
		//Utility::Debug(F(","));
		//Utility::Debug(b, HEX);
		//Utility::Debugln(F(")"));
	#endif
	
	#ifdef TRACKCHANGES
		// make them all dirty ... every bulb will change
		SetAllDirty();
	#endif

	byte* p = _pBulbData;
	for (byte i = 0; i < MAXBULBS; i++)
	{
		memset(p, b, OUTPUTSTRINGS);
		memset(p+OUTPUTSTRINGS, g, OUTPUTSTRINGS);
		memset(p+OUTPUTSTRINGS+OUTPUTSTRINGS, r, OUTPUTSTRINGS);
		p += OUTPUTSTRINGS * 3;
	}			

	#ifdef DEBUGSERIAL
		//DumpBulbData();
	#endif
}

DEFINE_TRANSPOSEBULBDATA_FN(TRANSPOSEBULBDATA)
DEFINE_CEPROTOCOL_FN_IO(MYCEPROTOCOLP, PINPORT)

void BulbTracker::Display()
{
	#ifdef DEBUGSERIAL
		Utility::Debugln(F("BulbTracker::Display. **********************"));
		Utility::Debug(F("F"));
		Utility::Debugln(Utility::FreeRam());
	#endif

	#ifdef DEBUGSERIAL
		//DumpBulbData();
	#endif
	for (byte i = 0; i < MAXBULBS; i++)
	{
		#ifdef TRACKCHANGES
		if (IsDirty(i))
		{
		#endif
			#ifdef DEBUGSERIAL
				//Utility::Debug(F("BulbTracker:: Pre transpose bulb "));
				//Utility::Debugln(i);
				//Utility::Debug(F("BulbData: "));
				//for (byte k = 0; k < OUTPUTSTRINGS*3; k++)
				//{
				//	Utility::Debug(*(_pBulbData + (i * OUTPUTSTRINGS * 3) + k), HEX);
				//	Utility::Debug(F(" "));
				//}
				//Utility::Debugln(F(""));
			#endif

			// set the address
			byte mask =0x20;
			for (byte j = 0; j < 6; j++)
			{
				if ((i & mask) > 0)
				{
					_pSendBuffer[1 + j] = 0xFF;
				}
				else
				{
					_pSendBuffer[1 + j] = 0x00;
				}
				mask = mask >> 1;
			}
			
			// Copy across the updated data into the send buffer
			TRANSPOSEBULBDATA(_pBulbData + (i * OUTPUTSTRINGS * 3), _pSendBuffer + 15, OUTPUTSTRINGS);

			#ifdef DEBUGSERIAL
				//DumpSendBuffer();
			#endif
			MYCEPROTOCOLP(_pSendBuffer, 1 + 6 + 8 + 3 * BITSPERCOLOUR);
		#ifdef TRACKCHANGES
		}
		#endif
	}


	#ifdef TRACKCHANGES
		SetAllClean();
	#endif
}

bool BulbTracker::ProcessDMX(DMX* pDMX)
{
	// check this universe is one we understand
	byte universe = pDMX->Universe();
	if (universe < UNIVERSE || universe > UNIVERSE + _UNIVERSES - 1)
	{
		Utility::Debugln(F("    BulbTracker universe rejected ... not one we are processing."));
		return false;
	}

	#ifdef DEBUGSERIAL
		Utility::Debug(F("BulbTracker processing DMX packet for universe "));
		Utility::Debugln(pDMX->Universe());
		Utility::Debug(F("Last Universe? "));
		Utility::Debugln(_LASTUNIVERSE(universe));
	#endif

	#ifdef DEBUGDUMPPIN
	bool dump = !digitalRead(DEBUGDUMPPIN);
	#endif

	byte rgb[3];
	
	#ifdef DEBUGDUMPPIN
		if (dump)
		{
			Utility::Debug(F("    DMX Packet dump for universe "));
			Utility::Debugln(universe);
		}
	#endif

	// for each string packed into this universe
	for (byte j = 0;  j < _STRINGSPERUNIVERSE; j++) 
	{
		if (_LASTUNIVERSE(universe) && j >= _STRINGSINLASTUNIVERSE)
		{
			// this does not exist
			break;
		}
	
		#ifdef DEBUGSERIAL
			Utility::Debug(F("    String in universe "));
			Utility::Debugln((universe - UNIVERSE) * _STRINGSPERUNIVERSE + j);
		#endif
		
		// calculate the string pointer
		byte* p = _pBulbData + (universe - UNIVERSE) * _STRINGSPERUNIVERSE + j;
	
		byte bulb  = 0;
		
		while (bulb < MAXBULBS)
		{
			if (pDMX->Read(&rgb[0], sizeof(rgb)))
			{
				for (int ptol = 0; ptol < PHYSICALTOLOGICALRATIO; ptol++)
				{
					byte* p2 = p + bulb * OUTPUTSTRINGS * 3;
				
					#ifdef DEBUGSERIAL
						//Utility::Debug(F("BulbTracker::Setting bulb "));
						//Utility::Debug(i);
						//Utility::Debug(F(" to "));
						//Utility::Debug(r, HEX);
						//Utility::Debug(F(","));
						//Utility::Debug(g, HEX);
						//Utility::Debug(F(","));
						//Utility::Debugln(b, HEX);
					#endif

					#ifdef DEBUGDUMPPIN
						if (dump)
						{
							Utility::Debug(F("("));
							Utility::Debug(rgb[0], HEX);
							Utility::Debug(F(","));
							Utility::Debug(rgb[1], HEX);
							Utility::Debug(F(","));
							Utility::Debug(rgb[2], HEX);
							Utility::Debug(F(")"));
						}
					#endif

					if (*(p2) != rgb[2] ||
						*(p2 + OUTPUTSTRINGS) != rgb[1] ||
						*(p2 + OUTPUTSTRINGS + OUTPUTSTRINGS) != rgb[0])
					{
						#ifdef TRACKCHANGES
							SetDirty(bulb);
						#endif
						*(p2) = rgb[2];
						*(p2 + OUTPUTSTRINGS) = rgb[1];
						*(p2 + OUTPUTSTRINGS + OUTPUTSTRINGS) = rgb[0];

						#ifdef DEBUGDUMPPIN
							if (dump)
							{
								Utility::Debug(F("*"));
							}
						#endif
					}
					bulb++;
				}
			}
			else
			{
				// we are out of packet data so exit
				break;
			}
		}
	}

	#ifdef DEBUGDUMPPIN
		if (dump)
		{
			Utility::Debugln(F(""));
		}
	#endif

	#ifdef DEBUGSERIAL
		Utility::Debugln(F("    BulbTracker::done processing DMX Message."));
	#endif
	
	// we handled this packer
	return true;
}