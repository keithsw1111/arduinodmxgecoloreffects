#ifndef UTILITY_H
	#define UTILITY_H

	#ifdef USEENC28J60
		#include <UIPEthernet.h>
	#else
		#include <Ethernet.h>
	#endif

	class Utility
	{
		public:
		static int FreeRam();
		static void Error(int err);
			
		static void DebugInit();
		static void Debug(const __FlashStringHelper* s);
		static void Debug(int i);
		static void Debug(byte b, int fmt);
		static void Debugln(const __FlashStringHelper* s);
		static void Debugln(int i);
		static void Debugln(IPAddress ip);
		static void Debugln(byte b, int fmt);

		static void sprintf(char* buffer, const __FlashStringHelper* message, ...);

		static void Reset();
	};
#endif