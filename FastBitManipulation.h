// Fast Bit Manipulation

#ifndef FASTBITMANIPULATION_H

	#define FASTBITMANIPULATION_H

	// InputData is a buffer in format RRRRRRRRGGGGGGGGBBBBBBBBB with BULBS repetitions. First R is bulb 1string 1. Second R is bulb 1 string 2 etc. Actual number of Rs etc depends on number of strings
	// OuputData is a buffer in the format Bulb1Bit1R, BULB2Bit2R .... Bulb1Bit8R, Bulb1Bit1G
	// BULB is the bulbs to transpose
	// STRINGS is the number of strings
	// r2-r9 hold the output bytes for a colour
	// r23 colour loop
	// r21 string loop
	// __tmp_reg__ byte being transposed
	
	#define TRANSPOSE(INPUTDATA, OUTPUTDATA, STRINGS) \
		asm volatile( \
		"    ldi r23, 3             ; 3 colours to do r23 \n" \
		" 1: mov r21, %[strings]    ; I need to extract bits for each string r21 \n" \
		"    sub r2, r2             ; clear output values \n " \
		"    sub r3, r3             ; \n " \
		"    sub r4, r4             ; \n " \
		"    sub r5, r5             ; \n " \
		" 2: ld __tmp_reg__, Y+     ; Get the input byte => __tmp_reg__ \n" \
		"    lsl __tmp_reg__        ; Extract 1sr bit to Carry flag \n" \
		"    rol r2           	    ; Load carry flag into r2 \n" \
		"    lsl __tmp_reg__        ; Extract 2nd bit to carry flag \n" \
		"    rol r3                 ; Load carry flag into r3 \n" \
		"    lsl __tmp_reg__        ; Extract 2rd bit to carry flag \n" \
		"    rol r4                 ; Load carry flag into r4 \n" \
		"    lsl __tmp_reg__        ; Extract 4th bit to carry flag \n" \
		"    rol r5                 ; Load carry flag into r5 \n" \
		"    dec r21                ; are we done \n" \
		"    cpi r21, 0             ; \n" \
		"    brne 2b                ; loop not done \n" \
		"    st Z+, r2              ; store the result\n" \
		"    st Z+, r3              ; \n" \
		"    st Z+, r4              ; \n" \
		"    st Z+, r5              ; \n" \
		"    dec r23                ; are we done \n" \
		"    cpi r23, 0             ; \n" \
		"    brne 1b                ; loop not done \n" \
		"3:                         ; \n" \
		: \
		: [inputdata] "y" (INPUTDATA), \
		  [outputdata] "z" (OUTPUTDATA), \
		  [strings] "r" (STRINGS) \
		: "r2", "r3", "r4", "r5", "r21", "r23", "cc", "memory" \
		)

	#define DEFINE_TRANSPOSEBULBDATA_FN(NAME) \
	extern void NAME(byte *INPUTDATA, byte *OUTPUTDATA, uint8_t STRINGS) __attribute__((noinline)); \
	void NAME(byte *INPUTDATA, byte *OUTPUTDATA, uint8_t STRINGS) { TRANSPOSE(INPUTDATA, OUTPUTDATA, STRINGS); }

#endif 

