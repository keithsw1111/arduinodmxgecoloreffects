/*
 * GE ColorEffects LED driver.
 */

// this code
// 0 - 10us low + 20us high
// 1 - 20us low + 10us high
// first 10us always low ... next 10us opposite of value ... next 10us always high

// 4 cycles = 0.25us ... 1 cycle = 0.0625us .. 10us = 160 cycles

#ifndef CEPROTOCOL_H

	#define CEPROTOCOL_H

	// len is the number of bytes of data ... this needs to be wrapped with a high prefix and low suffix.
	#define CEPROTOCOL_IO(PORT, RGB, LEN) \
		asm volatile( \
		"    cp %A[len], r1              ; 1 compare buffer length to I assume 0 \n" \
		"    cpc %B[len], r1             ; 1 compare carrying over result from previous compare \n" \
		"    brne 1f                     ; 1 false /2 true branch if not equal to main output routine \n" \
		"    rjmp 8f                     ; 2 jump to end \n" \
		"mydelay: push r21               ; 2 save register \n" \
		"         ldi r21, 10             ; 1 times to loop \n" \
		"3:       nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         nop                    ; 1 do nothing \n" \
		"         dec r21                ; 1 decrement count \n" \
		"         cpi r21,0              ; 1 check if zero \n" \
		"         brne 3b                ; 1 true/2 false loop until zero \n" \
		"         nop                    ; 1 do nothing \n" \
		"         pop r21                ; 2 restore register \n" \
		"         ret                    ; 4 return from subroutine \n" \
		"1:  in r26, __SREG__            ; 1 read from __SEREG__ ? \n" \
		"    cli                         ; 1 global interrupt disable \n" \
		"    ldi r20, 255                ; 1 HIGH \n" \
		"    ldi r21, 0                  ; 1 LOW \n" \
		"2:  out %[port], r21            ; 1 (hold for 160 cycles - 10us)\n" \
		"    ld r19, Z+                  ; 1 read the data we are to send \n" \
		"    com r19                     ; 1 not the value of r19 because of the coloreffects protocol \n"\
		"    call mydelay                ; 3 call my delay routine \n" \
		"    nop                         ; 1 do nothing \n" \
		"    nop                         ; 1 do nothing \n" \
		"    out %[port], r19            ; 1 (hold for 160 cycles - 10us) \n" \
		"    call mydelay                ; 3 call my delay routine \n" \
		"    nop                         ; 1 do nothing \n" \
		"    nop                         ; 1 do nothing \n" \
		"    nop                         ; 1 do nothing \n" \
		"    nop                         ; 1 do nothing \n" \
		"    out %[port], r20            ; 1 (hold for 160 cycles - 10us) \n" \
		"    call mydelay                ; 3 call my delay routine \n" \
		"    sbiw %A[len], 1             ; 2 subtract 1 from buffer length \n" \
		"    breq 9f                     ; 1 false / 2 ture exit if zero after subtraction \n" \
		"    rjmp 2b                     ; 2 loop back and process the next byte \n" \
		"9:  out %[port], r21            ; 1 Leave the line low \n" \
		"    sei                         ; 1 enable global interrupts \n " \
		"    out __SREG__, r26           ; 1 write to __SEREG__\n" \
		"8:                              ; end \n" \
		: \
		: [rgb] "z" (RGB), \
		  [len] "w" (LEN), \
		  [port] "I" (_SFR_IO_ADDR(PORT)) \
		: "r19", "r20", "r21", "r26", "r27", "cc", "memory" \
		)

		#define DEFINE_CEPROTOCOL_FN_IO(NAME, PORT) \
		extern void NAME(byte *rgb, uint16_t len) __attribute__((noinline)); \
		void NAME(byte *rgb, uint16_t len) { CEPROTOCOL_IO(PORT, rgb, len); }

#endif 

