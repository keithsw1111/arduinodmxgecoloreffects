#include "global.h"
#include "FlashStringHelper.h"
#include "myethernet.h"

void(* resetFunc) (void) = 0;//declare reset function at address 0

void Utility::Reset()
{
	resetFunc();
}

void Utility::sprintf(char* buffer, const __FlashStringHelper* message, ...)
{
	// potential buffer overflow
	FlashStringHelper* pfsh = new FlashStringHelper(message);
	va_list args;
	va_start(args, pfsh->GetString());
	vsprintf(buffer, pfsh->GetString(), args);
	va_end(args);
	delete pfsh;
}

void Utility::DebugInit()
{
	#if defined(DEBUGSERIAL) 
		Serial.begin(115200);
		Serial.println(F("Serial debug initialised."));
		Serial.flush();
	#endif
}

void Utility::Debug(const __FlashStringHelper* s)
{
	#if defined(DEBUGSERIAL) 
		Serial.print(s);
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(FlashStringHelper::Length(s) + 1);
		Utility::sprintf(p, s);
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

void Utility::Debug(int i)
{
	#if defined(DEBUGSERIAL) 
		Serial.print(i);
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(10);
		Utility::sprintf(p, F("%i"), i);
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

void Utility::Debug(byte b, int fmt)
{
	#if defined(DEBUGSERIAL) 
		Serial.print(b, fmt);
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(10);
		Utility::sprintf(p, F("%b"), b);
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

void Utility::Debugln(const __FlashStringHelper* s)
{
	#if defined(DEBUGSERIAL) 
		Serial.println(s);
		Serial.flush();
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(FlashStringHelper::Length(s) + 2);
		Utility::sprintf(p, s);
		strcat(p, "\n");
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

void Utility::Debugln(IPAddress ip)
{
	#if defined(DEBUGSERIAL) 
		Serial.println(ip);
		Serial.flush();
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(20);
		Utility::sprintf(p, F("%b.%b.%b.%b\n"), *((byte*)&ip), *((byte*)(&ip)+1), *((byte*)(&ip)+2), *((byte*)(&ip)+3));
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

void Utility::Debugln(int i)
{
	#if defined(DEBUGSERIAL) 
		Serial.println(i);
		Serial.flush();
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(16);
		Utility::sprintf(p, F("%i\n"), i);
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

void Utility::Debugln(byte b, int fmt)
{
	#if defined(DEBUGSERIAL) 
		Serial.println(b, fmt);
		Serial.flush();
	#endif
	
	if (debugudp)
	{
		// Send to udp
		char* p = (char*)malloc(10);
		Utility::sprintf(p, F("%b\n"), b);
		myethernet->SendDebugPacket(p);
		free(p);
	}
}

int Utility::FreeRam ()
{
	extern int __heap_start, *__brkval;
	int v;
	return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

void Utility::Error(int err)
{
	while(true)
	{
		Utility::Debug(F("Error: "));
		Utility::Debugln(err);
		
		for(int i = 0; i < err; i++)
		{
			digitalWrite(ERRORPIN, 1);
			delay(100);
			digitalWrite(ERRORPIN, 0);
			delay(100);
		}
		delay(1000);
	}
}
